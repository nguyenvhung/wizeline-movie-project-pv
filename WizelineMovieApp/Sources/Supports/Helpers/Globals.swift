
import UIKit
import Foundation

var DefaultTextColor: UIColor = UIColor.color4B(red: 22, green: 22, blue: 22, anpha: 255)
var DefaultSecondTextColor: UIColor = UIColor(hexaString: "#5B5752")
var DefaultSoftTextColor: UIColor = UIColor(hexaString: "#847F78")
var DefaultDisableTextColor: UIColor = UIColor(hexaString: "#847F78")
var DefaultUnReadNotificationColor: UIColor = UIColor(hexaString: "#ECF0D6")
var DefaultNotificationBorderColor: UIColor = UIColor(hexaString: "#CED6AF")
var DefaultIconColor: UIColor = UIColor(hexaString: "#A5A099")
var DefaultRedColor: UIColor = UIColor(hexaString: "#DE7266")
var DefaultBorderColor: UIColor = UIColor(hexaString: "#C8C0B6")
var DefaultBorderColorDark: UIColor = UIColor(hexaString: "#5B5752")
var DefaultHyperLinkColor: UIColor = UIColor(red: 70 / 255, green: 171 / 255, blue: 243 / 255, alpha: 1)
var StatusBarGray: UIColor = UIColor(red: 83 / 255, green: 88 / 255, blue: 95 / 255, alpha: 1.0)
var DefaultGrayBackground: UIColor = UIColor(hexaString: "#EAE3D9")
var DefaultBlackBackground: UIColor = UIColor(hexaString: "#222222")
var DefaultBlueBackground: UIColor = UIColor(hexaString: "#46ABF3")
var DefaultLightBackground: UIColor = UIColor(hexaString: "#C8C0B6", alpha: 0.4)
var DefaultInactiveButtonBackground: UIColor = UIColor(hexaString: "#000000", alpha: 0.33)
var DefaultPlaceholderCollectionBackground: UIColor = UIColor(red: 200.0/255.0, green: 192.0 / 255.0, blue: 182.0/255.0, alpha: 0.4)
var DefaultImageModeShow = UIView.ContentMode.scaleAspectFill
var DefaultBorderWidth: CGFloat = 0.5

let DefaultInviteButtonColor = UIColor(hexaString: "#443932");

class Global {
    
    
    //Fontsize
    static let defaultHeadlineFontSize: CGFloat = 24
    static let defaultHeadlineFontSizeBig: CGFloat = 28
    static let defaultHeadlineFontSizeSmall: CGFloat = 20
    static let defaultFontSize: CGFloat = 14
    static let defaultFontSizeSmall: CGFloat = 12
    static let defaultFontSizeSmallest: CGFloat = 12
    static let defaultFontSizeBig: CGFloat = 16


    static var notificationTimer: Timer!
    static let defaultMargin: CGFloat = 8
    static let defaultBodyMargin: CGFloat = 48
    static var instance: Global!
    static let navigationBarHeight: CGFloat = 44 + 20
    static let bottomTabBarHeight: CGFloat = 54;
    static var cacheImages: [String: UIImage] = [:]
    static var recipeButtonGroupHeight: CGFloat {
        get {
            return 45 + Global.defaultMargin * 2
        }
    }
    static var statusBarHeight: CGFloat {
        get {
            if #available(iOS 11.0, *), Global.safeAreaInset.top != 0 {
                return Global.safeAreaInset.top
            } else {
                return 20
            }
        }
    }
    static var safeBottomHeight: CGFloat {
        get {
            if Global.safeAreaInset.bottom > 0 {
                return Global.safeAreaInset.bottom + Global.navigationBarHeight - 16
            } else {
                return Global.safeAreaInset.bottom + Global.navigationBarHeight
            }
        }
    }
    static var safeAreaInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    
    static func createUILabelWithFont(text: String, fontSize: CGFloat = Global.defaultFontSize, fontWeight: UIFont.Weight = .regular) -> UILabel {
        let lb = UILabel()
        lb.text = text
        lb.font = UIFont.systemFont(ofSize: fontSize, weight: fontWeight)
        lb.textColor = DefaultTextColor
        return lb
    }
    
    static func printJsonOjb(_ inputData: Any?) {
        guard let inputData = inputData else {
            return
        }
        let data = inputData as AnyObject
        if data.isKind(of: NSData.self) {
            let someText = String(data: data as! Data, encoding: String.Encoding.utf8)
            if let someText = someText {
                print(someText);
            }
        } else {
            let someText: String? = String(data: try! JSONSerialization.data(withJSONObject: data, options: .prettyPrinted), encoding: .utf8)
            if let someText = someText {
                print(someText);
            } else {
                print(data)
            }
        }
    }
    
}
