////
////  ExtensionUIView.swift
////
//
import UIKit
import PureLayout

extension UIImageView {
    static var defaultLoadingImage = UIImage(named: "default_placeholder")
    
}

extension UIView {
    func toRoundRect() {
        self.toRoundRect(cornerRadius: 5.0, borderColor: UIColor.lightGray, borderWidth: DefaultBorderWidth)
    }
    func toRoundRect(cornerRadius: CGFloat, borderColor: UIColor, borderWidth: CGFloat) {
        self.clipsToBounds = true
        self.layer.cornerRadius = cornerRadius
        self.layer.borderWidth = borderWidth
        self.layer.borderColor = borderColor.cgColor
    }
    func toRoundRect(cornerRadius: CGFloat, borderColor: UIColor) {
        self.toRoundRect(cornerRadius: cornerRadius, borderColor: borderColor, borderWidth: DefaultBorderWidth)
    }
    func removeAllSubViews() {
        for view in self.subviews {
            view.removeFromSuperview()
        }
    }
    
   
     
        func removeAllConstraints() {
            self.removeConstraints(self.constraints)
        }
     
//
//    var width: CGFloat {
//        get {
//            return self.frame.size.width
//        }
//        set(newWidth) {
//            self.frame.size.width = newWidth
//        }
//    }
//    var height: CGFloat {
//        get {
//            return self.frame.size.height
//        }
//        set(newHeight) {
//            self.frame.size.height = newHeight
//        }
//    }
//    var top: CGFloat {
//        get {
//            return self.frame.origin.y
//        }
//        set(newTop) {
//            self.frame.origin.y = newTop
//        }
//    }
//    var left: CGFloat {
//        get {
//            return self.frame.origin.x
//        }
//        set(newLeft) {
//            self.frame.origin.x = newLeft
//        }
//    }
//    var topWithHeight: CGFloat {
//        get {
//            return self.frame.size.height +  self.frame.origin.y
//        }
//    }
//    var leftWithWidth: CGFloat {
//        get {
//            return self.frame.size.width + self.frame.origin.x
//        }
//    }
//    var parentViewController: UIViewController? {
//        var parentResponder: UIResponder? = self
//        while parentResponder != nil {
//            parentResponder = parentResponder!.next
//            if let viewController = parentResponder as? UIViewController {
//                return viewController
//            }
//        }
//        return nil
//    }
//
//    func parentView<T: UIView>(of type: T.Type) -> T? {
//        guard let view = superview else {
//            return nil
//        }
//        return (view as? T) ?? view.parentView(of: T.self)
//    }
//
//
//    func resizeToFitSubviews() {
//        let subviewsRect = subviews.reduce(CGRect.zero) {
//            $0.union($1.frame)
//        }
//
//        let fix = subviewsRect.origin
//        subviews.forEach {
//            let _ = $0.frame.insetBy(dx: -fix.x, dy: -fix.y)
//        }
//
//        let _ = frame.insetBy(dx: fix.x, dy: fix.y)
//        frame.size = subviewsRect.size
//    }
//    func resizeToFitSubviews(fixedWidth: CGFloat) {
//        self.resizeToFitSubviews()
//        self.frame.size.width = fixedWidth
//    }
//    func resizeToFitSubviews(fixedHeight: CGFloat) {
//        self.resizeToFitSubviews()
//        self.frame.size.height = fixedHeight
//    }
//    func resizeToFitSubviews(fixedWidth: CGFloat, fixedHeight: CGFloat) {
//        self.resizeToFitSubviews()
//        self.frame.size.width = fixedWidth
//        self.frame.size.height = fixedHeight
//    }
//    func resizeHeightToFitSubviews(addtionHeight: CGFloat) {
//        var newHeight: CGFloat = 0
//        for view in subviews {
//            if view.topWithHeight > newHeight {
//                newHeight = view.topWithHeight
//            }
//        }
//        self.height = newHeight + addtionHeight
//    }
//    func resizeHeightToFitSubviews() {
//        self.resizeHeightToFitSubviews(addtionHeight: 0)
//    }
//    func removeAllSubViews() {
//        for view in self.subviews {
//            view.removeFromSuperview()
//        }
//    }
//    func topBorder(color: UIColor, borderWidth: CGFloat, offset: CGFloat = 0) {
//        self.viewWithTag(21)?.removeFromSuperview()
//        let border = UIView()
//        border.tag = 21
//        border.backgroundColor = color
//        border.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleBottomMargin]
//        border.frame = CGRect(x: 0, y: 0 - offset, width: self.width, height: borderWidth)
//        self.addSubview(border)
//    }
//    func bottomBorder(color: UIColor, borderWidth: CGFloat, offset: CGFloat = 0) {
//        self.viewWithTag(23)?.removeFromSuperview()
//        let border = UIView.newAutoLayout()
//        border.tag = 23
//        border.backgroundColor = color
//        self.addSubview(border)
//        border.autoSetDimension(.height, toSize: borderWidth)
//        border.autoPinEdgeLeftAndRightToSupperView(withOffset: 0)
//        border.autoPinEdge(toSuperviewEdge: .bottom, withInset: offset)
//    }
//    func leftBorder(color: UIColor, borderWidth: CGFloat, offset: CGFloat = 0) {
//        self.viewWithTag(24)?.removeFromSuperview()
//        let border = UIView()
//        border.tag = 24
//        border.backgroundColor = color
//        border.autoresizingMask = [UIView.AutoresizingMask.flexibleHeight, UIView.AutoresizingMask.flexibleRightMargin]
//        border.frame = CGRect(x: 0 + offset, y: 0, width: borderWidth, height: self.height)
//        self.addSubview(border)
//    }
//    func rightBorder(color: UIColor, borderWidth: CGFloat, offset: CGFloat = 0) {
//        self.viewWithTag(22)?.removeFromSuperview()
//        let border = UIView()
//        border.tag = 22
//        border.backgroundColor = color
//        border.autoresizingMask = [UIView.AutoresizingMask.flexibleHeight, UIView.AutoresizingMask.flexibleLeftMargin]
//        border.frame = CGRect(x: self.width - borderWidth - offset, y: 0, width: borderWidth, height: self.height)
//        self.addSubview(border)
//    }
//    func toRoundRect() {
//        self.toRoundRect(cornerRadius: 5.0, borderColor: UIColor.lightGray, borderWidth: 1.0)
//    }
//    func toRoundRect(cornerRadius:CGFloat, borderColor: UIColor, borderWidth: CGFloat) {
//        self.clipsToBounds = true
//        self.layer.cornerRadius = cornerRadius
//        self.layer.borderWidth = borderWidth
//        self.layer.borderColor = borderColor.cgColor
//    }
//    func toRoundRect(cornerRadius:CGFloat, borderColor: UIColor) {
//        self.toRoundRect(cornerRadius: cornerRadius, borderColor: borderColor, borderWidth: 1.0)
//    }
//
//    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
//        if #available(iOS 11, *) {
//            self.clipsToBounds = true
//            self.layer.cornerRadius = radius
//            var masked = CACornerMask()
//            if corners.contains(.topLeft) { masked.insert(.layerMinXMinYCorner) }
//            if corners.contains(.topRight) { masked.insert(.layerMaxXMinYCorner) }
//            if corners.contains(.bottomLeft) { masked.insert(.layerMinXMaxYCorner) }
//            if corners.contains(.bottomRight) { masked.insert(.layerMaxXMaxYCorner) }
//            self.layer.maskedCorners = masked
//        }
//        else {
//            let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
//            let mask = CAShapeLayer()
//            mask.path = path.cgPath
//            layer.mask = mask
//        }
//    }
//
//    func dimissAllKeyBoard() {
//        self.doDimimissAllKeyBoard(views: self.subviews)
//    }
//    private func doDimimissAllKeyBoard(views: [UIView]) {
//        for view in views {
//            if view.isKind(of: UITextView.self) || view.isKind(of: UITextField.self) {
//                view.resignFirstResponder()
//            }
//            self.doDimimissAllKeyBoard(views: view.subviews)
//        }
//    }
//
//    func fadeIn(duration: TimeInterval) {
//        UIView.animate(withDuration: duration) {
//            self.alpha = 1
//        }
//    }
//
//    func fadeOut(duration: TimeInterval) {
//        UIView.animate(withDuration: duration) {
//            self.alpha = 0
//        }
//    }
//
//    func setBoxShadow(color: UIColor = .black, radius: CGFloat = 5) {
//        self.layer.shadowColor = color.cgColor
//        self.layer.shadowOpacity = 1
//        self.layer.shadowOffset = .zero
//        self.layer.shadowRadius = radius
//    }
//
    @discardableResult
    func autoPinEdgeTopToBottom(of view: UIView, withOffset offset: CGFloat) -> NSLayoutConstraint {
        return self.autoPinEdge(.top, to: .bottom, of: view, withOffset: offset)
    }
    @discardableResult
    func autoPinEdgeLeftAndRightToSupperView(withOffset offset: CGFloat) -> [NSLayoutConstraint] {
        var layouts: [NSLayoutConstraint] = []
        layouts.append(self.autoPinEdge(toSuperviewEdge: .left, withInset: offset))
        layouts.append(self.autoPinEdge(toSuperviewEdge: .right, withInset: offset))
        return layouts
    }
//
//    func rotate(angle: CGFloat) {
//        self.transform = self.transform.rotated(by: CGFloat(CGFloat.pi * angle / 180));
//    }
//
//    func updateLayout() {
//        self.setNeedsLayout()
//        self.layoutIfNeeded()
//    }
//
//    func addBlurEffect(style: UIBlurEffect.Style = .regular) {
//        backgroundColor = .clear
//        let blurEffect = UIBlurEffect(style: style)
//        let blurView = UIVisualEffectView(effect: blurEffect)
//
//        let vibrancyEffect = UIVibrancyEffect(blurEffect: blurEffect)
//        let vibrancyEffectView = UIVisualEffectView(effect: vibrancyEffect)
//        blurView.contentView.addSubview(vibrancyEffectView)
//        vibrancyEffectView.autoPinEdgesToSuperviewEdges()
//        blurView.isUserInteractionEnabled = false
//        blurView.backgroundColor = .clear
//        blurView.layer.cornerRadius = self.layer.cornerRadius
//        blurView.layer.masksToBounds = true
//        self.insertSubview(blurView, at: 0)
//        blurView.autoPinEdgesToSuperviewEdges()
//    }
}
