//
//  String+Delimiter.swift
//  survey_hung
//
//  Created by Hung Nguyen on 19/10/2021.
//

import Foundation

extension String {
    func toStringsArray(delimiter: String = ",") -> [String] {
        components(separatedBy: delimiter).map {
            $0.trimmingCharacters(in: .whitespacesAndNewlines)
        }
    }
    
    func toNilIfEmpty() -> String? {
        isEmpty ? nil : self
    }
    
    enum InputRegex: String{
        case email = "^[A-Za-z\\d@.+]{0,255}$"
        case password = "^[A-Za-z\\d!@#$%^&*()_+]{0,30}$"
        case otp = "^[\\d]{0,6}$"
        case phone = "^[\\d+]{0,16}$"
    }
    
    func capitalizingFirstLetter() -> String {
      return prefix(1).uppercased() + self.lowercased().dropFirst()
    }

    func capitalizeFirstLetter() -> String {
      return self.capitalizingFirstLetter()
    }
    
    func base64Encoded() -> String? {
        return data(using: .utf8)?.base64EncodedString()
    }
 

    func urlEncoded() -> String {
        let urlAllowed: CharacterSet = .alphanumerics.union(.init(charactersIn: "-._~"))
        return self.addingPercentEncoding(withAllowedCharacters: urlAllowed) ?? ""
    }
    
    func getSubString(from: Int, to: Int) -> String {
        var offsetTo = to
        if to >= self.count {
            offsetTo = self.count - 1
        }
        if to < 0 {
            offsetTo = 0
        }
        var offsetFrom = from
        if from < 0 {
            offsetFrom = 0
        }
        if offsetFrom > offsetTo {
            let dump = offsetFrom
            offsetFrom = offsetTo
            offsetTo = dump
        }

        let startIndex = self.index(self.startIndex, offsetBy: offsetFrom)
        let endIndex = self.index(self.startIndex, offsetBy: offsetTo)
        return String(self[startIndex...endIndex])
    }
    func toInt() -> Int {
        return Int(self.toFloat())
    }
    func toFloat() -> Float {
        return (self as NSString).floatValue
    }
    
    func parseToInt() -> Int? {
        return Int(self.components(separatedBy: CharacterSet.decimalDigits.inverted).joined())
    }

    func containLineBreak() -> Bool {
        return self.contains("\n")
    }
    
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return nil }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return nil
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }

    func isValidEmail() -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"

        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: self)
    }

    func ranges(of substring: String, options: CompareOptions = [], locale: Locale? = nil) -> [Range<Index>] {
        var ranges: [Range<Index>] = []
        while let range = self.range(of: substring, options: options, range: (ranges.last?.upperBound ?? self.startIndex) ..< self.endIndex, locale: locale) {
            ranges.append(range)
        }
        return ranges
    }

    func extractUrls() -> [URL] {
        let types: NSTextCheckingResult.CheckingType = .link
        do {
            let detector = try NSDataDetector(types: types.rawValue)
            let matches = detector.matches(in: self, options: .reportCompletion, range: NSMakeRange(0, self.count))
            return matches.compactMap({$0.url})
        } catch let _ {
            return []
        }
    }
    
    func isNumber() -> Bool {
        let allowedCharacters = ".1234567890"
        let allowedCharcterSet = CharacterSet(charactersIn: allowedCharacters)
        let typedCharcterSet = CharacterSet(charactersIn: self)
        return allowedCharcterSet.isSuperset(of: typedCharcterSet)
    }
}
