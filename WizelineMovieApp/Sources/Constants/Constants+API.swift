//
//  Constants+API.swift
//

enum Environment  {
    case dev
    case staging
    case production
}

fileprivate func currentEnv() -> Environment {
    #if Debug
        return .staging
    #endif
        return .production // Return PROD if env is not found.
}


extension Constants.API {
    static var baseURL: String {
        switch currentEnv() {
           case .dev: return "https://api.themoviedb.org/3"
           case .staging: return "https://api.themoviedb.org/3"
           case .production: return "https://api.themoviedb.org/3"
        }
    }
    
    static var baseURLForImage: String {
        switch currentEnv() {
           case .dev: return "https://image.tmdb.org/t/p"
           case .staging: return "https://image.tmdb.org/t/p"
           case .production: return "https://image.tmdb.org/t/p"
        }
    }
    
    static let apiKey: String = "fce9002a14d2a9df6121f300d03601c7"
}


