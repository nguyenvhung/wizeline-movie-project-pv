//
//  MovieRepostory.swift
//  WizelineMovieApp
//
//  Created by hungnguy on 4/28/22.
//

import Foundation
import RxSwift

protocol MovieRepositoryProtocol {
    func getTrendingMoviesList(window: GetTrendingsMediaWindow) -> Single<[MovieModel]>
}

final class MovieRepository: MovieRepositoryProtocol {
    private let networkAPI: NetworkAPIProtocol
    private let authenticatedNetworkAPI: AuthenticatedNetworkAPIProtocol

    init(networkAPI: NetworkAPIProtocol,
         authenticatedNetworkAPI: AuthenticatedNetworkAPIProtocol) {
        self.networkAPI = networkAPI
        self.authenticatedNetworkAPI = authenticatedNetworkAPI
    }
    
    func getTrendingMoviesList(window: GetTrendingsMediaWindow) -> Single<[MovieModel]> {
        let requestConfiguration = AuthorisedRequestConfiguration.getTrendings(window: window)
        return networkAPI
            .performRequest(requestConfiguration, for: APIMoviesResponse.self)
            .map({ data in
                debugPrint("performRequest done: \(data.results)")
                return data.results ?? []
            })
    }
}
