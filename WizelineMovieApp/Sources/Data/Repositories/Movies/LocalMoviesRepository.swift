//
//  LocalMoviesRepository.swift
//  WizelineMovieApp
//
//  Created by hungnguy on 4/29/22.
//

import Foundation
import RxSwift
import RealmSwift
import Realm

protocol LocalMoviesRepositoryProtocol {
    func getMoviesListFromDatabase() -> Single<[MovieModel]>
}

final class LocalMoviesRepository: LocalMoviesRepositoryProtocol {
    private let realm = try! Realm()

    init() {}
    
    func getMoviesListFromDatabase( ) -> Single<[MovieModel]> {
        let datum = realm.objects(MovieRealmModel.self)
        var toReturn: [MovieModel] = []
        for each in datum {
            toReturn.append(each.toMovieModel())
        }
        return Single.just(toReturn)
    }
}
