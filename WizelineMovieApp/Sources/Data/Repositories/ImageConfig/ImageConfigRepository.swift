//
//  ImageConfigRepository.swift
//  WizelineMovieApp
//
//  Created by hungnguy on 4/29/22.
//

import Foundation
import RxSwift

protocol ImageConfigRepositoryProtocol {
    func getConfigs() -> Single<ConfigurationModel>
}

final class ImageConfigRepository: ImageConfigRepositoryProtocol {
    private let networkAPI: NetworkAPIProtocol
    private let authenticatedNetworkAPI: AuthenticatedNetworkAPIProtocol

    init(networkAPI: NetworkAPIProtocol,
         authenticatedNetworkAPI: AuthenticatedNetworkAPIProtocol) {
        self.networkAPI = networkAPI
        self.authenticatedNetworkAPI = authenticatedNetworkAPI
    }
    
    func getConfigs() -> Single<ConfigurationModel> {
        let requestConfiguration = AuthorisedRequestConfiguration.getConfiguration
        return networkAPI
            .performRequest(requestConfiguration, for: APIConfigResponse.self)
            .map({ data in
                debugPrint("performRequest done: \(data.images?.toJSON())")
                return data.images ?? ConfigurationModel()
            })
    }
}

