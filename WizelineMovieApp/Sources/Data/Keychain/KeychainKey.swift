//
//  KeychainKey.swift
//  survey_hung
//
//  Created by Hung Nguyen on 30/08/2021.
//

protocol KeychainKey {}

extension KeychainKey {

    static var user: Keychain.Key<String> {
        Keychain.Key(key: "user")
    }
    
    static var hung: Keychain.Key<String> {
        Keychain.Key(key: "hung")
    }
}
