//
//  AuthenticatedInterceptor.swift
//  survey_hung
//
//  Created by Hung Nguyen on 29/09/2021.
//

import Alamofire
import Foundation

final class AuthenticatedInterceptor: RequestInterceptor {

    private let keychain: KeychainProtocol

    init(keychain: KeychainProtocol) {
        self.keychain = keychain
    }

    func adapt(_ urlRequest: URLRequest, for session: Session, completion: @escaping (Result<URLRequest, Error>) -> Void) {
        var request = urlRequest
        if let accessToken = try? keychain.get(.user) {
            request.headers.add(name: "Authorization", value: "Bearer \(accessToken)")
        }
        completion(.success(request))
    }

    func retry(_ request: Request, for session: Session, dueTo error: Error, completion: @escaping (RetryResult) -> Void) {
        print("\(#function) : request : \(request), retryCount : \(request.retryCount)")
        if let status = request.response?.statusCode, status == 401 { // 401 = unauthorized
            NotificationCenter.default.post(name: .unauthorized, object: nil)
        }
        completion(.doNotRetry)
    }
}

extension NSNotification.Name {
    static let unauthorized = NSNotification.Name("unauthorized")
}
