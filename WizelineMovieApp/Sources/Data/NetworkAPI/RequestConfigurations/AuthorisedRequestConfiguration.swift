//
//  AuthenticationRequestConfiguration.swift
//  survey_hung
//

//

import Alamofire

enum GetTrendingsMediaWindow: String {
    case day = "day"
    case week = "week"
}

enum AuthorisedRequestConfiguration {
    case getTrendings(window: GetTrendingsMediaWindow)
    case getConfiguration
    case getImage(imageNameWithExtension: String)
}

extension AuthorisedRequestConfiguration: RequestConfiguration {

    var baseURL: String {
        switch self {
        case .getImage(_):
            return  Constants.API.baseURLForImage
        default:
            return  Constants.API.baseURL
        }
    }

    var endpoint: String {
        var originalString = ""
        switch self {
        case .getTrendings(let window):
            originalString = "/trending/all/\(window.rawValue)?api_key=\(Constants.API.apiKey)"
        case .getConfiguration:
            originalString = "/configuration?api_key=\(Constants.API.apiKey)"
        case .getImage(let imageNameWithExtension):
            originalString = "/w500/\(imageNameWithExtension)"
        }
        return originalString
    }

    var method: HTTPMethod {
        switch self {
        default:
            return .get
        }
    }

    var parameters: Parameters? { // Params for POST methods if Yes
        switch self {
        default:
            return nil
        }
    }

    var encoding: ParameterEncoding { URLEncoding.default }
}
