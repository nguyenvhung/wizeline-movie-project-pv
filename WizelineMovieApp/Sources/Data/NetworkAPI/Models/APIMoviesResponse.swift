//
//  APISurveysResponse.swift
//  survey_hung
//
//  Created by hungnguy on 4/20/22.
//  Copyright © 2022 Hung Nguyen. All rights reserved.
//

import Foundation
import ObjectMapper 

class APIMoviesResponse: Mappable, Decodable {
    var results: [MovieModel]?
    
    init() {}
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        results <- map["results"]
    }
}

