//
//  APIUserResponse.swift
//  survey_hung
//

//

import Foundation
import ObjectMapper

class APIConfigResponse: Decodable, Mappable {
//    let images: ConfigurationModel
    
    var images: ConfigurationModel?
    
    init() {}
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        images <- map["images"]
    }
} 
