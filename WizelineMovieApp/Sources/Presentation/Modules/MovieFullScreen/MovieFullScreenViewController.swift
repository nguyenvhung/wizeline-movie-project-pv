//
//  MovieFullScreenViewController.swift
//  WizelineMovieApp
//
//  Created by hungnguy on 4/29/22.
//

import Foundation
import UIKit
import PureLayout
import Kingfisher

class MovieFullScreenViewController: UIViewController {
    var imageUrlString: String
    var movie: MovieModel
    
    private var blurEffectView: UIVisualEffectView!
    
    init(imageUrlString: String, movie: MovieModel) {
        self.imageUrlString = imageUrlString
        self.movie = movie
         super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        makeViews()
    }
    
    
    private func makeViews() {
        view.backgroundColor = .white
        
        self.title = movie.getDisplayableTitle()
        
        let uiImageView = UIImageView(image: UIImage(named: "default_placeholder"))
        uiImageView.contentMode = .scaleAspectFit
        self.view.addSubview(   uiImageView)
        uiImageView.autoPinEdgesToSuperviewEdges()
        
        let url = URL(string: imageUrlString)!
        
        uiImageView.kf.setImage(with: url, placeholder: UIImage(named: "default_placeholder"))
        showBlurrableLoadingIndicatorOverlay()
    }
    
    private func showBlurrableLoadingIndicatorOverlay() {
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.dark)
        blurEffectView = UIVisualEffectView(effect: blurEffect)
        
        let label = Global.createUILabelWithFont(text: movie.getDisplayableTitle())
        blurEffectView.contentView.addSubview(label)
        label.autoPinEdgesToSuperviewEdges()
        
        view.addSubview(blurEffectView)
        blurEffectView.autoSetDimensions(to: CGSize(width: UIScreen.main.bounds.width, height: 100))
        blurEffectView.autoPinEdgesToSuperviewEdges(with: .zero, excludingEdge: .top)
        
        if #available(iOS 11.0, *) {
            let window = UIApplication.shared.keyWindow
            let bottomPadding = window?.safeAreaInsets.bottom
            
            blurEffectView.autoSetDimension(.height, toSize: 100 + (bottomPadding ?? 0))
            
        }
    }
    
    
}
