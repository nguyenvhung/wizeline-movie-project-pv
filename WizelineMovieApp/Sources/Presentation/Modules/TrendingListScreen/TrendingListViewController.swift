//
//  HomeViewController.swift
//  survey_hung
//
//  Created by hungnguy on 4/15/22.
//  Copyright © 2022 Hung Nguyen. All rights reserved.
//

import UIKit
import RxSwift
import SwifterSwift
import Resolver

enum ViewAs: String {
    case grid = "Grid"
    case list = "List"
}

final class TrendingListViewController: UIViewController {
    var viewModel: TrendingListViewModelProtocol = Resolver.resolve()
    var disposeBag = DisposeBag()
    
    var blurEffectView: UIView!
    let spinner = UIActivityIndicatorView(style: .whiteLarge)
    
    var emailField: UITextField!
    var psswrdField: UITextField!
    var moviesCollectionView: WaterfallCollectionView!
    
    var movies: [MovieModel] = []
    var imageConfig: ConfigurationModel?
    
    var initViewAs: ViewAs = .grid
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "Trending"
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addTapped))
    
        let refresh = UIBarButtonItem(barButtonSystemItem: .refresh, target: self, action: #selector(refreshMovieList))
        let list = UIBarButtonItem(title: "View", style: .plain, target: self, action: #selector(changeListTypeTapped))

        navigationItem.rightBarButtonItems = [list, refresh]
    }
    
    @objc private func refreshMovieList() {
        self.movies.removeAll()
        self.getMoviesList()
    }
    
    @objc private func changeListTypeTapped() {
        if self.initViewAs == .list {
            self.initViewAs = .grid
        } else {
            self.initViewAs = .list
        }
        self.moviesCollectionView.setNumberOfColumns(numberOfColumns: self.initViewAs == .grid ? .two : .one)
    }
    
    @objc private func addTapped() {
        let vc = AddNewImageViewController()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    private func showBlurrableLoadingIndicatorOverlay() {
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.dark)
        self.blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = view.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        spinner.translatesAutoresizingMaskIntoConstraints = false
        spinner.startAnimating()
        UIView.animate(withDuration: 1, delay: 2){
            self.view.addSubview(self.blurEffectView)
            self.view.addSubview(self.spinner)
            self.spinner.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
            self.spinner.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
        }
    }
    
    private func hideBlurrableLoadingIndicatorOverlay() {
        UIView.animate(withDuration: 1, delay: 2) {
            self.blurEffectView?.removeFromSuperview()
            self.spinner.stopAnimating()
            self.spinner.removeFromSuperview()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        debugPrint("It is here.")
        
        self.movies.removeAll()
        
        makeViews()
        didNeedRefreshImgConfig()
    }
    
    private func makeViews() {
        view.backgroundColor = .white
        moviesCollectionView = WaterfallCollectionView(scrollable: true, numberOfColumns: .two)
        
        self.view.addSubview(moviesCollectionView)
        moviesCollectionView.autoPinEdgesToSuperviewEdges()
    }
    
    @objc private func didNeedRefreshImgConfig() {
        viewModel.input.didNeedToRefreshGlobalConfig()
        viewModel.output.didGetConfigSuccess.asObservable()
            .subscribe(onNext:  { [weak self] config in
                guard let self = self else { return }
                self.hideBlurrableLoadingIndicatorOverlay()
                self.imageConfig  = config
                self.getMoviesList()
            }).disposed(by: disposeBag)
    }
    
    @objc private func getMoviesList() {
        viewModel.input.didTapLoginButton(email:   "", password:  "")
    
        self.showBlurrableLoadingIndicatorOverlay()
        
        viewModel.output.didGetMoviesSuccess
            .asObservable()
            .subscribe(onNext:  { [weak self] movies in
                guard let self = self else { return }
                debugPrint("count for now = \(movies.count)")
                self.hideBlurrableLoadingIndicatorOverlay()
                self.movies.append(contentsOf: movies)
                if let param = self.imageConfig?.posterSizes?[0] {
                    self.moviesCollectionView.setImageWidthParam(param)
                }
                self.moviesCollectionView.setItems(items: self.movies)
                self.moviesCollectionView.setNumberOfColumns(numberOfColumns: .two)
                
            }).disposed(by: disposeBag)
        
        viewModel.output.didGetMoviesDBSuccess
            .asObservable()
            .subscribe(onNext:  { [weak self] movies in
                guard let self = self else { return }
                debugPrint("count for now = \(movies.count)")
                self.hideBlurrableLoadingIndicatorOverlay()
                self.movies.append(contentsOf: movies)
                if let param = self.imageConfig?.posterSizes?[0] {
                    self.moviesCollectionView.setImageWidthParam(param)
                }
                self.moviesCollectionView.setItems(items: self.movies)
                self.moviesCollectionView.setNumberOfColumns(numberOfColumns: .two)
                
            }).disposed(by: disposeBag)
    }
    
    private func showDialog(message: String ) {
        let alert = UIAlertController(title: "Oops",
                                      message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    private func makeLogInTextField(placeholderText: String = "") -> UITextField {
        let textField = UITextField.newAutoLayout()
        textField.borderStyle = .none
        textField.layer.cornerRadius = 14.0
        textField.attributedPlaceholder = NSAttributedString(
            string: placeholderText,
            attributes: [NSAttributedString.Key.foregroundColor: UIColor.white.withAlphaComponent(0.3)]
        )
        textField.tintColor = .white
        textField.textColor = .white
        let padding = UIView(frame: CGRect(x: 0, y: 0, width: 12, height: textField.frame.height))
        textField.leftView = padding
        textField.leftViewMode = .always
        textField.autoSetDimension(.height, toSize: 56.0)
        return textField
    }
}
