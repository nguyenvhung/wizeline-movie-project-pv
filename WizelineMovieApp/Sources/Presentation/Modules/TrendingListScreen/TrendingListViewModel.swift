//
//  HomeViewModel.swift
//  survey_hung
//
//  Created by hungnguy on 4/17/22.
//  Copyright © 2022 Hung Nguyen. All rights reserved.
//

import Foundation
import Resolver
import RxCocoa
import RxSwift

// sourcery: AutoMockable
protocol TrendingListViewModelInput {
    func didTapLoginButton(email: String, password: String)
    func didNeedToRefreshGlobalConfig()
}

// sourcery: AutoMockable
protocol TrendingListViewModelOutput {
    var didGetMoviesSuccess: PublishSubject<[MovieModel]> { get }
    var didGetMoviesFailed: Signal<Error> { get }
    
    var didGetMoviesDBSuccess: PublishSubject<[MovieModel]> { get }
    var didGetMoviesDBFailed: Signal<Error> { get }
    
    var didGetConfigSuccess: PublishSubject<ConfigurationModel>{ get }
    var didGetConfigFailed: PublishSubject<Error>{ get }
}

// sourcery: AutoMockable
protocol TrendingListViewModelProtocol {
    var input: TrendingListViewModelInput { get }
    var output: TrendingListViewModelOutput { get }
}

final class TrendingListViewModel: TrendingListViewModelProtocol {
    
    typealias LoginParams = (email: String, password: String)
    
    private let disposeBag = DisposeBag()
    private let getMoviesTrigger = PublishRelay<LoginParams>()
    private let getMoviesFromDBTrigger = PublishRelay<Void>()
    private let getConfigTrigger = PublishRelay<Void>()
    
    private var doneRetrievedUserAfterLogin = PublishSubject<Void>()
    private var failedRetrievedUserAfterLogin = PublishSubject<Error>()
    
    private var doneGetMovies = PublishSubject<[MovieModel]>()
    private var failedGetMovies = PublishSubject<Error>()
    
    private var doneGetMoviesFromDB = PublishSubject<[MovieModel]>()
    private var failedGetMoviesFromDB = PublishSubject<Error>()
    
    private var doneGetConfig = PublishSubject<ConfigurationModel>()
    private var failedGetConfig = PublishSubject<Error>()
    
    var input: TrendingListViewModelInput { self }
    var output: TrendingListViewModelOutput { self }
    
    @Injected var getMoviesUseCase: MoviesUseCasesProtocol
    @Injected var getConfigurationUseCase: ImageConfigUseCaseProtocol
    
    init() {
        getMoviesTrigger
            .withUnretained(self)
            .flatMapLatest { owner, inputs in owner.getMoviesTriggered(email: inputs.email, password: inputs.password) }
            .subscribe()
            .disposed(by: disposeBag)
        
        getMoviesFromDBTrigger
            .withUnretained(self)
            .flatMapLatest { owner, inputs in owner.getMoviesFromDBTriggered() }
            .subscribe()
            .disposed(by: disposeBag)
        
        getConfigTrigger
            .withUnretained(self)
            .flatMapLatest { owner, inputs in owner.getImageConfig() }
            .subscribe()
            .disposed(by: disposeBag)
    }
    
    private func getMoviesTriggered(email: String, password: String) -> Observable<[MovieModel]> {
            getMoviesUseCase.listMovies(window: .day).asObservable().do(
            onNext: { datum in
                self.doneGetMovies.onNext(datum)
                debugPrint("count = \(datum.count)")
            },
            onError: { error in
                debugPrint("This failed \(error.localizedDescription)")
                self.failedGetMovies.onNext(error)
            },
            onCompleted: {}
        )
            }
    
    private func getMoviesFromDBTriggered() -> Observable<[MovieModel]> {
        getMoviesUseCase.listMoviesFromDatabase().asObservable().do(
            onNext: { datum in
                self.doneGetMoviesFromDB.onNext(datum)
                debugPrint("count = \(datum.count)")
            },
            onError: { error in
                debugPrint("This failed \(error.localizedDescription)")
                self.failedGetMoviesFromDB.onNext(error)
            },
            onCompleted: {}
        )
            }

    
    private func getImageConfig() -> Observable<ConfigurationModel> {
        getConfigurationUseCase.getConfig().asObservable()
            .do(
                onNext: { data in
                    self.doneGetConfig.onNext(data)
                    
                    GlobalImageConfiguration.instance.savedConfig = data
                },
                onError: { error in
                    debugPrint("This failed \(error.localizedDescription)")
                    self.failedGetConfig.onNext(error)
                },
                onCompleted: {}
            )
    }
}

extension TrendingListViewModel: TrendingListViewModelInput {
    func didTapLoginButton(email: String, password: String) {
        getMoviesTrigger.accept((email, password))
        getMoviesFromDBTrigger.accept(())
    }
    
    func didNeedToRefreshGlobalConfig()  {
        getConfigTrigger.accept(())
    }
}

extension TrendingListViewModel: TrendingListViewModelOutput {
    var didGetMoviesDBSuccess: PublishSubject<[MovieModel]> {
        return self.doneGetMoviesFromDB
    }
    
    var didGetMoviesDBFailed: Signal<Error> {
        return failedGetMoviesFromDB.asSignal(onErrorJustReturn: NSError())
    }
    
    var didGetMoviesSuccess: PublishSubject<[MovieModel]> {
        return self.doneGetMovies
    }
    
    var didGetMoviesFailed: Signal<Error> {
        return self.failedRetrievedUserAfterLogin.asSignal(onErrorJustReturn: NSError())
    }
    
    var didGetConfigSuccess: PublishSubject<ConfigurationModel> {
        return self.doneGetConfig
    }
    
    var didGetConfigFailed: PublishSubject<Error> {
        return self.failedGetConfig
    }
}
