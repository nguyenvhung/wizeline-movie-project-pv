//
//  AddNewImageViewController.swift
//  WizelineMovieApp
//
//  Created by hungnguy on 4/29/22.
//

import Foundation
import UIKit
import RealmSwift
import Realm

class AddNewImageViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    var titleField: UITextField!
    var descriptionField: UITextField!
    var imageViewPic: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.setNavigationBarHidden(false, animated: true)
        makeViews()
    }
    
    
    private func makeViews() {
        view.backgroundColor = .white
        
        let containerViewOfFields = UIView.newAutoLayout()
        view.addSubview(containerViewOfFields)
        let window = UIApplication.shared.windows.filter {$0.isKeyWindow}.first
        let height = window?.windowScene?.statusBarManager?.statusBarFrame.height ?? 0
        containerViewOfFields.autoPinEdge(toSuperviewEdge: .top, withInset: height + 60)
        containerViewOfFields.autoPinEdgeLeftAndRightToSupperView(withOffset: 0)
        
        titleField = makeLogInTextField(placeholderText: "Title")
        containerViewOfFields.addSubview(titleField)
        titleField.backgroundColor = UIColor.gray.withAlphaComponent(0.18)
        titleField.autoPinEdge(toSuperviewEdge: .top)
        titleField.autoPinEdgeLeftAndRightToSupperView(withOffset: 24.0)
        titleField.keyboardType = .emailAddress
        titleField.autocapitalizationType = .none
        titleField.autocorrectionType = .no
        
        descriptionField = makeLogInTextField(placeholderText: "Description")
        containerViewOfFields.addSubview(descriptionField)
        descriptionField.backgroundColor = UIColor.gray.withAlphaComponent(0.18)
        descriptionField.autoPinEdgeTopToBottom(of: titleField, withOffset: 16.0)
        descriptionField.autoPinEdgeLeftAndRightToSupperView(withOffset: 24.0)
        
        let pickImageButton = UIButton.newAutoLayout()
        pickImageButton.setTitle("Pick Image", for: .normal)
        pickImageButton.titleLabel?.font = UIFont.systemFont(ofSize: 17.0, weight: .bold)
        pickImageButton.setTitleColor(.black, for: .normal)
        pickImageButton.autoSetDimension(.height, toSize: 56.0)
        pickImageButton.layer.cornerRadius = 14.0
        containerViewOfFields.addSubview(pickImageButton)
        pickImageButton.autoPinEdgeLeftAndRightToSupperView(withOffset: 24.0)
        pickImageButton.autoPinEdgeTopToBottom(of: descriptionField, withOffset: 16.0)
        pickImageButton.addTarget(self, action: #selector(imagePickerBtnAction), for: .touchUpInside)
        
        let saveBtn = UIButton.newAutoLayout()
        saveBtn.setTitle("Save", for: .normal)
        saveBtn.titleLabel?.font = UIFont.systemFont(ofSize: 17.0, weight: .bold)
        saveBtn.setTitleColor(.black, for: .normal)
        saveBtn.autoSetDimension(.height, toSize: 56.0)
        saveBtn.layer.cornerRadius = 14.0
        containerViewOfFields.addSubview(saveBtn)
        saveBtn.autoPinEdgeLeftAndRightToSupperView(withOffset: 24.0)
        saveBtn.autoPinEdgeTopToBottom(of: pickImageButton, withOffset: 16.0)
        saveBtn.autoPinEdge(toSuperviewEdge: .bottom)
        saveBtn.addTarget(self, action: #selector(didTapSave), for: .touchUpInside)
        
        imageViewPic = UIImageView(image: UIImage(named: "default_placeholder"))
        imageViewPic.contentMode = .scaleAspectFit
        view.addSubview(imageViewPic)
        imageViewPic.autoPinEdgeLeftAndRightToSupperView(withOffset: Global.defaultMargin*2)
        imageViewPic.autoPinEdgeTopToBottom(of: containerViewOfFields, withOffset: Global.defaultMargin*2)
        imageViewPic.autoPinEdge(toSuperviewEdge: .bottom)
    }
    
    //MARK:-- ImagePicker delegate
        func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
            if let pickedImage = info[.originalImage] as? UIImage {
                imageViewPic.image = pickedImage
                imageViewPic.contentMode = .scaleAspectFit
            }
        picker.dismiss(animated: true, completion: nil)
    }
    
    @objc func didTapSave() {
        // Use them like regular Swift objects
        let newMovie = MovieRealmModel()
        newMovie.title = titleField.text ?? ""
        newMovie.overview = descriptionField.text ?? ""
        newMovie.setPosterDataFromUIImage(image: (imageViewPic.image)!)

        // Get the default Realm
        let realm = try! Realm()

        // Persist your data easily
        try! realm.write {
            realm.add(newMovie)
        }
        let movies = realm.objects(MovieRealmModel.self)
        debugPrint("Saved to Realm: \(movies.count)")
        
        navigationController?.popViewController()
    }
    
    //MARK:- Image Picker
    @objc func imagePickerBtnAction( )
        {

            let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
            alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
                self.openCamera()
            }))

            alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
                self.openGallery()
            }))

            alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))

            self.present(alert, animated: true, completion: nil)
        }
    
    func openCamera()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallery()
   {
       if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary){
           let imagePicker = UIImagePickerController()
           imagePicker.delegate = self
           imagePicker.allowsEditing = true
           imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
           self.present(imagePicker, animated: true, completion: nil)
       }
       else
       {
           let alert  = UIAlertController(title: "Warning", message: "You don't have permission to access gallery.", preferredStyle: .alert)
           alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
           self.present(alert, animated: true, completion: nil)
       }
   }
    
    
    private func makeLogInTextField(placeholderText: String = "") -> UITextField {
        let textField = UITextField.newAutoLayout()
        textField.borderStyle = .none
        textField.layer.cornerRadius = 14.0
        textField.attributedPlaceholder = NSAttributedString(
            string: placeholderText,
            attributes: [NSAttributedString.Key.foregroundColor: UIColor.black.withAlphaComponent(0.6)]
        )
        let padding = UIView(frame: CGRect(x: 0, y: 0, width: 12, height: textField.frame.height))
        textField.leftView = padding
        textField.leftViewMode = .always
        textField.autoSetDimension(.height, toSize: 56.0)
        return textField
    }
}
