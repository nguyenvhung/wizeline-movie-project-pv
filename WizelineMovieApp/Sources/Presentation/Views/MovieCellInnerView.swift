//
//  MovieCellInnerView.swift
//  WizelineMovieApp
//
//  Created by hungnguy on 4/28/22.
//

import Foundation
import UIKit
import PureLayout
import Kingfisher

class WaterfallCollectionContentView: UIView {
    var containerView: UIView!
    var imageView: UIView!
    var captionLabel: UILabel!
    var dateLabel: UILabel!
    var iconViewOnLabel: UIImageView!
    var topLeftIconView: UIView!
    var topRightIconView: UIView!
    var profileNameAndAvatar: UIView!
    
    var insetValueForIcon: CGFloat = 6.0
    var imageSize: CGFloat = 18.0
    
    required init?(coder: NSCoder) {
        fatalError("coder: init")
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    private func makeMainImageView(movieItem: MovieModel) -> UIView {
        if (movieItem.posterPath ?? "").count > 0 {
            return PosterImageView(movie: movieItem, size: .zero)
        }
        
        let view = UIView.newAutoLayout()
        let collectionDefaultImage = Global.createUILabelWithFont(text: "🎬", fontSize: 52)
        view.addSubview(collectionDefaultImage)
        collectionDefaultImage.autoCenterInSuperview()
        view.toRoundRect(cornerRadius: 8, borderColor: DefaultBorderColor, borderWidth: DefaultBorderWidth)
        return view
    }
    
    func makeView(movieItem: MovieModel, size: CGSize, indexAsString: String = "") {
        self.removeAllSubViews()
        self.removeAllConstraints()
        
        imageView = self.makeMainImageView(movieItem: movieItem)
        self.addSubview(imageView)
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.autoPinEdge(toSuperviewEdge: .left)
        imageView.autoPinEdge(toSuperviewEdge: .right)
        imageView.autoPinEdge(toSuperviewEdge: .top)
        imageView.autoMatch(.height, to: .width, of: self, withMultiplier: size.height/size.width)
        
        captionLabel = Global.createUILabelWithFont(text: "\(indexAsString)\(movieItem.getDisplayableTitle())", fontSize: 14)
        captionLabel.numberOfLines = 2
        captionLabel.font = UIFont.systemFont(ofSize: 14, weight: .semibold)
        self.addSubview(captionLabel)
        captionLabel.autoPinEdge(.top, to: .bottom, of: imageView, withOffset: 8)
        captionLabel.autoPinEdge(toSuperviewEdge: .left)
        captionLabel.autoPinEdge(toSuperviewEdge: .right, withInset: 0)
        
        dateLabel = Global.createUILabelWithFont(text: "\(indexAsString)\(movieItem.releaseDate ?? "")", fontSize: 14)
        dateLabel.numberOfLines = 2
        dateLabel.font = UIFont.preferredFont(forTextStyle: .caption1)
        self.addSubview(dateLabel)
        dateLabel.autoPinEdge(.top, to: .bottom, of: captionLabel, withOffset: 8)
        dateLabel.autoPinEdge(toSuperviewEdge: .left)
        dateLabel.autoPinEdge(toSuperviewEdge: .right, withInset: 0)
    }
    
    func makeRoundBackgrounedImageView(image: UIImage?) -> UIView {
        let uiimageView = UIImageView.newAutoLayout()
        uiimageView.autoSetDimensions(to: CGSize(width: Global.defaultMargin*2, height: Global.defaultMargin*2))
        uiimageView.layer.cornerRadius =  Global.defaultMargin + insetValueForIcon
        uiimageView.clipsToBounds = true
        uiimageView.backgroundColor = .white
        uiimageView.contentMode = .center
        uiimageView.image = image?.withAlignmentRectInsets(UIEdgeInsets(top: insetValueForIcon, left: insetValueForIcon,
                                                                        bottom: insetValueForIcon, right: insetValueForIcon))
        return uiimageView
    }
}


class PosterImageView: UIImageView {
     
        var movie: MovieModel!
        var userId: Int = 0
    
        override init(frame: CGRect) {
            super.init(frame: .zero)
        }
    
        init(movie: MovieModel, size: CGSize) {
            super.init(image: nil)
            self.width = size.width
            self.height = size.height
            self.contentMode = .scaleAspectFill
            self.movie = movie
            self.image = UIImageView.defaultLoadingImage
            
            let bestFit = getBestWidthBasedOnScreen()
            
            if (movie.posterPath ?? "").count > 0 {
                let url = URL(string: "https://image.tmdb.org/t/p/\(bestFit)\(movie.posterPath ?? "")")!
                self.kf.setImage(with: url, placeholder: UIImageView.defaultLoadingImage)
            }
            
            if let data = movie.posterData {
                debugPrint("data = data")
                self.image = UIImage(data: data)
            }
            
            self.clipsToBounds = true
            self.autoSetDimensions(to: size)

            self.toRoundRect(cornerRadius: Global.defaultMargin, borderColor: .clear)
            self.initTouch()
        }
    
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        private func initTouch() {
            let touch = UITapGestureRecognizer.init(target: self, action: #selector(processTouch))
            self.isUserInteractionEnabled = true
            self.addGestureRecognizer(touch)
        }
    
        @objc func processTouch() {
            debugPrint("This is to touch PosterImageView")
            if let parentVC = self.parentViewController {
                
                var vc = MovieFullScreenViewController(imageUrlString: "https://image.tmdb.org/t/p/w500\(movie.posterPath ?? "")",
                                                       movie: self.movie
                )
                parentVC.navigationController?.pushViewController(vc, animated: true)
            }
        }
        
        func disableTouch() {
            self.isUserInteractionEnabled = false
        }
    
    
    func getBestWidthBasedOnScreen() -> String {
        if GlobalImageConfiguration.instance.savedConfig.posterSizes.isNilOrEmpty {
            return "w500"
        } else {
            let widthList = GlobalImageConfiguration.instance.savedConfig.posterSizes?.compactMap { return Int.parse(from: $0) }
            var bestFit = 0
            if (widthList ?? []).isEmpty { return "w500" } else {
                bestFit = widthList![0]
                for each in widthList! {
                    if CGFloat(each) < (UIScreen.main.bounds.width - Global.defaultMargin*2) {
                        bestFit = each
                    }
                }
            }
            print("bestfit = w\(bestFit)")
            return "w\(bestFit)"
        }
    }

}

fileprivate extension Int {
    static func parse(from string: String) -> Int? {
        return Int(string.components(separatedBy: CharacterSet.decimalDigits.inverted).joined())
    }
}
