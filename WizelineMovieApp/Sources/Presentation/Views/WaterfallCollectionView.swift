//
//  WaterfallCollectionView.swift
//  WizelineMovieApp
//
//  Created by hungnguy on 4/28/22.
//

import Foundation
import UIKit
import PureLayout
import Kingfisher
import SwiftUI

protocol WaterfallCollectionDataDelegate: AnyObject {
    func waterfallCollectionRefresh(_ collectionView: UICollectionView, callback: ([MovieModel])->())
    func waterfallCollectionLoadMore(_ collectionView: UICollectionView, callback: ([MovieModel])->())
}

class WaterfallCollectionView: UICollectionView, WaterfallCollectionViewLayoutDelegate, UICollectionViewDelegate, UICollectionViewDataSource {
     
    
    weak var dataDelegate: WaterfallCollectionDataDelegate?
    
    private var movieItems: [MovieModel] = []
    private var userId: Int?
    private var page: Int = 1
    private var lastItemReach: Bool = false
    
    private var imageWidthParam: String = "w500"
    
    private let ownedRefreshControl = UIRefreshControl()
    
    private let noImagePlaceHolderImageSize: CGSize = CGSize(width: 160, height: 80)
    
    private var identifier: String = "WaterfallCollectionViewCell"
    private var currentCollectionViewSize: CGSize = .init(width: UIScreen.main.bounds.width, height: .zero)
    private var numberOfColumns: NumberOfColumns?
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    init(scrollable: Bool = false, numberOfColumns: NumberOfColumns) {
        let layout = WaterfallCollectionViewLayout(numberOfColumns: numberOfColumns)
        
        super.init(frame: .zero, collectionViewLayout: layout)
        layout.waterFallStyleDelegate = self
        self.numberOfColumns = numberOfColumns
        self.setCollectionViewLayout(layout, animated: true)
        self.delegate = self
        self.dataSource = self
        self.isScrollEnabled = scrollable
        self.register(WaterfallCollectionViewCell.self, forCellWithReuseIdentifier: identifier)
        self.contentInset = UIEdgeInsets(top: 0,
                                         left: Global.defaultMargin * 2,
                                         bottom: Global.safeBottomHeight + Global.navigationBarHeight + Global.defaultMargin * 2,
                                         right: Global.defaultMargin * 2)
        self.refreshControl = ownedRefreshControl
        refreshControl?.addTarget(self, action: #selector(refreshData(_:)), for: .valueChanged)
    }
    
    @objc func refreshData(_ sender: Any) {
        self.dataDelegate?.waterfallCollectionRefresh(self) { items in
            self.movieItems = items
            self.refreshControl?.endRefreshing()
            self.reloadData()
        }
    }
    
    func setNumberOfColumns(numberOfColumns: NumberOfColumns) {
        let layout = WaterfallCollectionViewLayout(numberOfColumns: numberOfColumns)
        layout.waterFallStyleDelegate = self
        self.setCollectionViewLayout(layout, animated: true)
        self.reloadData()
        self.reloadSections(IndexSet(integer: 0))
    }
    
    func setImageWidthParam(_ param: String = "w500") {
        self.imageWidthParam = param
        self.reloadData()
        self.reloadSections(IndexSet(integer: 0))
    }
    
    func setItems(items: [MovieModel]) {
        self.movieItems = items
        self.reloadData()
        self.reloadSections(IndexSet(integer: 0))
    }
    
    private func determineThumbnailSize( ) -> CGSize {
        let width = UIScreen.main.bounds.width - Global.defaultMargin * 4
        return CGSize(width: width, height: ( self.numberOfColumns == .two) ? (width * 1.5) : (width/2))
    }
    
    private func getTextSizeWithAFont(fontWeight: UIFont.Weight, fontSize: CGFloat, text: String) -> CGSize {
        let font = UIFont.systemFont(ofSize: fontSize, weight: fontWeight)
        let fontAttributes = [NSAttributedString.Key.font: font]
        let size = (text as NSString).size(withAttributes: fontAttributes as [NSAttributedString.Key : Any])
        return size
    }
    
    private func getBottomInfoBlockHeight(movieItem: MovieModel) -> CGFloat {
        let item = movieItem
        var toReturn = Global.defaultMargin * 4
        let maxWidthOfColumn = (UIScreen.main.bounds.width / 2  - Global.defaultMargin * 3 - 18)
        if self.getTextSizeWithAFont(fontWeight: .semibold, fontSize: 14.0, text: item.getDisplayableTitle()).width > maxWidthOfColumn {
            toReturn = toReturn + Global.defaultMargin * 2
        }
        if (movieItem.releaseDate ?? "").count > 0 {
            toReturn = toReturn + Global.defaultMargin + 18
        }
        return toReturn
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return movieItems.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let item = movieItems[indexPath.item]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifier,
                                                      for: indexPath as IndexPath) as! WaterfallCollectionViewCell
        cell.makeView(movieItem: item, size: self.determineThumbnailSize())
        if lastItemReach == false && indexPath.item == (movieItems.count - 1) {
                dataDelegate?.waterfallCollectionLoadMore(self) { items in
                    self.movieItems.append(contentsOf: items)
                    self.reloadData()
                }
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, heightForPhotoAtIndexPath indexPath: IndexPath) -> CGFloat {
            return determineThumbnailSize().height
    }
    
    func collectionView(_ collectionView: UICollectionView, heightOfLabelAtIndexPath indexPath: IndexPath) -> CGFloat {
        return self.getBottomInfoBlockHeight(movieItem: movieItems[indexPath.item])
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func sendCurrentHeight(_ collectionView: UICollectionView, ofIndexPath indexPath: IndexPath, height: CGFloat) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            self.currentCollectionViewSize.height = height
            self.removeAllConstraints()
            self.autoSetDimensions(to: self.currentCollectionViewSize)
        }
    }
}

class WaterfallCollectionViewCell: UICollectionViewCell {
    var imageSize: CGSize = .zero
    
    required init?(coder: NSCoder) {
        fatalError("coder: init")
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    func makeView(withInnerUIView: UIView, indexAsString: String = "") {
        contentView.addSubview(withInnerUIView)
        withInnerUIView.autoPinEdgesToSuperviewEdges()
    }
}

extension WaterfallCollectionViewCell {
    func makeView(movieItem: MovieModel, size: CGSize, indexAsString: String = "") {
        contentView.removeAllSubViews()
        contentView.removeAllConstraints()
        let view = WaterfallCollectionContentView()
        view.makeView(movieItem: movieItem, size: size, indexAsString: indexAsString)
        contentView.addSubview(view)
        view.autoPinEdgesToSuperviewEdges()
    }
}

// MARK: - WaterfallCollectionViewLayout:

protocol WaterfallCollectionViewLayoutDelegate: AnyObject {
    func collectionView(_ collectionView: UICollectionView, heightForPhotoAtIndexPath indexPath: IndexPath) -> CGFloat
    func collectionView(_ collectionView: UICollectionView, heightOfLabelAtIndexPath indexPath: IndexPath) -> CGFloat
    func sendCurrentHeight(_ collectionView: UICollectionView, ofIndexPath indexPath: IndexPath, height: CGFloat)
}

enum NumberOfColumns: Int {
    case one = 1
    case two = 2
}

class WaterfallCollectionViewLayout: UICollectionViewFlowLayout {
    weak var waterFallStyleDelegate: WaterfallCollectionViewLayoutDelegate?
    private var numberOfColumns: NumberOfColumns
    private let cellPadding: CGFloat = 0
    private var cache: [UICollectionViewLayoutAttributes] = []
    private var contentHeight: CGFloat = 0
    
    init(numberOfColumns: NumberOfColumns) {
        self.numberOfColumns = numberOfColumns
        super.init()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private var contentWidth: CGFloat {
        guard let collectionView = collectionView else {
            return 0
        }
        let insets = collectionView.contentInset
        return collectionView.bounds.width - (insets.left + insets.right)
    }
    
    override var collectionViewContentSize: CGSize {
        return CGSize(width: contentWidth, height: contentHeight)
    }
    
    var currentHeight = 0.0
    
    override func prepare() {
        guard let collectionView = collectionView
        else {
            return
        }
        let numberOfColumns = self.numberOfColumns.rawValue
        let columnWidth = contentWidth / CGFloat(numberOfColumns)
        var xOffset: [CGFloat] = []
        for column in 0..<numberOfColumns {
            xOffset.append(CGFloat(column) * columnWidth)
        }
        var column = 0
        var yOffset: [CGFloat] = .init(repeating: 0, count: numberOfColumns)
        
        for item in 0..<collectionView.numberOfItems(inSection: 0) {
            let indexPath = IndexPath(item: item, section: 0)
            
            let photoHeight = waterFallStyleDelegate?.collectionView(
                collectionView,
                heightForPhotoAtIndexPath: indexPath) ?? 180
            let labelHeight = waterFallStyleDelegate?.collectionView(
                collectionView,
                heightOfLabelAtIndexPath: indexPath) ?? 0
            let height = photoHeight / 2 + labelHeight + (numberOfColumns == 1 ? 100 : 0)
            let frame = CGRect(x: xOffset[column],
                               y: yOffset[column],
                               width: columnWidth,
                               height: height)
            var insetValue: UIEdgeInsets = indexPath.row % 2 == 0 ?
            UIEdgeInsets(top: 0, left: 0, bottom: 0, right: Global.defaultMargin) : UIEdgeInsets(top: 0, left: Global.defaultMargin, bottom: 0, right: 0)
            if numberOfColumns == 1 { insetValue = .zero }
            let insetFrame = frame.inset(by: insetValue)
 
            let attributes = UICollectionViewLayoutAttributes(forCellWith: indexPath)
            attributes.frame = insetFrame
            cache.append(attributes)
 
            contentHeight = max(contentHeight, frame.maxY)
            currentHeight = contentHeight

            yOffset[column] = yOffset[column] + height
            column = column < (numberOfColumns - 1) ? (column + 1) : 0
            waterFallStyleDelegate?.sendCurrentHeight(collectionView, ofIndexPath: indexPath, height: frame.maxY)
        }
    }
    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        var visibleLayoutAttributes: [UICollectionViewLayoutAttributes] = []
        for attributes in cache {
            if attributes.frame.intersects(rect) {
                visibleLayoutAttributes.append(attributes)
            }
        }
        return visibleLayoutAttributes
    }
    
    override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        return cache[indexPath.item]
    }
}

