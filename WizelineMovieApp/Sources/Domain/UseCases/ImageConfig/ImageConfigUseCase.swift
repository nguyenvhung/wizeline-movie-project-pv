//
//  ImageConfigUseCase.swift
//  WizelineMovieApp
//
//  Created by hungnguy on 4/29/22.
//

import Foundation
import Foundation
import ObjectMapper
import Apollo
import KeychainSwift
import RxSwift

// sourcery: AutoMockable
protocol ImageConfigUseCaseProtocol: AnyObject {
    func getConfig() -> Single<ConfigurationModel>
}

class ImageConfigUseCase: ImageConfigUseCaseProtocol {
    private let repository: ImageConfigRepositoryProtocol

    init(repository: ImageConfigRepositoryProtocol) {
        self.repository = repository
    }
    
    func getConfig() -> Single<ConfigurationModel> {
        repository
            .getConfigs()
            .map { response in
                return response
            }
    }
}
