//
//  MoviesUseCases.swift
//  WizelineMovieApp
//
//  Created by hungnguy on 4/28/22.
//

import Foundation
import ObjectMapper
import Apollo
import KeychainSwift
import RxSwift

// sourcery: AutoMockable
protocol MoviesUseCasesProtocol: AnyObject {
    func listMovies(window: GetTrendingsMediaWindow) -> Single<[MovieModel]>
    func listMoviesFromDatabase() -> Single<[MovieModel]>
}

class MoviesUseCases: MoviesUseCasesProtocol {
    private let remoteRepository: MovieRepositoryProtocol
    private let localDatabaseRepository: LocalMoviesRepositoryProtocol

    init(remoteRepository: MovieRepositoryProtocol, localDatabaseRepository: LocalMoviesRepositoryProtocol) {
        self.remoteRepository = remoteRepository
        self.localDatabaseRepository = localDatabaseRepository
    }
    
    func listMovies(window: GetTrendingsMediaWindow) -> Single<[MovieModel]> {
        remoteRepository
            .getTrendingMoviesList(window: window)
            .map { responses in
                return responses
            }
    }
    
    func listMoviesFromDatabase() -> Single<[MovieModel]> {
        localDatabaseRepository.getMoviesListFromDatabase()
            .map { responses in
                return responses
            }
    }
}
