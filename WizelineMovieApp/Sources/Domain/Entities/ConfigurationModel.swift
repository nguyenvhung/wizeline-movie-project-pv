//
//  ConfigurationMode.swift
//  WizelineMovieApp
//
//  Created by hungnguy on 4/28/22.
//

import Foundation
import ObjectMapper

class ConfigurationModel: Mappable, Equatable, Codable {
    var baseUrl: String?
    var secureBaseUrl: String?
    var backdropSizes: [String]?
    var posterSizes: [String]?
    var logoSizes: [String]?
    var profileSizes: [String]?
    var stillSizes: [String]?
    
    init() {}

    required init?(map: Map) {}

    func mapping(map: Map) {
          baseUrl <- map["base_url"]
          secureBaseUrl <- map["secure_base_url"]
          backdropSizes <- map["backdrop_sizes"]
          logoSizes <- map["logo_sizes"]
          posterSizes <- map["poster_sizes"]
          profileSizes <- map["profile_sizes"]
          stillSizes <- map["still_sizes"]
    }
    
    static func == (lhs: ConfigurationModel, rhs: ConfigurationModel) -> Bool {
        return lhs.baseUrl == rhs.baseUrl
    }
    
    func isEligible() -> Bool {
        return (posterSizes ?? []).isEmpty == false
    }
    
}


//"images": {
//    "base_url": "http://image.tmdb.org/t/p/",
//    "secure_base_url": "https://image.tmdb.org/t/p/",
//    "backdrop_sizes": [
//        "w300",
//        "w780",
//        "w1280",
//        "original"
//    ],
//    "logo_sizes": [
//        "w45",
//        "w92",
//        "w154",
//        "w185",
//        "w300",
//        "w500",
//        "original"
//    ],
//    "poster_sizes": [
//        "w92",
//        "w154",
//        "w185",
//        "w342",
//        "w500",
//        "w780",
//        "original"
//    ],
//    "profile_sizes": [
//        "w45",
//        "w185",
//        "h632",
//        "original"
//    ],
//    "still_sizes": [
//        "w92",
//        "w185",
//        "w300",
//        "original"
//    ]
//},
//"change_keys": [
//    "adult",
//    "air_date",
//    "also_known_as",
//    "alternative_titles",
//    "biography",
//    "birthday",
//    "budget",
//    "cast",
//    "certifications",
//    "character_names",
//    "created_by",
//    "crew",
//    "deathday",
//    "episode",
//    "episode_number",
//    "episode_run_time",
//    "freebase_id",
//    "freebase_mid",
//    "general",
//    "genres",
//    "guest_stars",
//    "homepage",
//    "images",
//    "imdb_id",
//    "languages",
//    "name",
//    "network",
//    "origin_country",
//    "original_name",
//    "original_title",
//    "overview",
//    "parts",
//    "place_of_birth",
//    "plot_keywords",
//    "production_code",
//    "production_companies",
//    "production_countries",
//    "releases",
//    "revenue",
//    "runtime",
//    "season",
//    "season_number",
//    "season_regular",
//    "spoken_languages",
//    "status",
//    "tagline",
//    "title",
//    "translations",
//    "tvdb_id",
//    "tvrage_id",
//    "type",
//    "video",
//    "videos"
//]
//}
