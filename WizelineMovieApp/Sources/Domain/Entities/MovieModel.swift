//
//  MovieModel.swift
//  WizelineMovieApp
//
//  Created by hungnguy on 4/28/22.
//

import Foundation
import ObjectMapper
import UIKit


class MovieModel: Mappable, Codable, Equatable {
    
    var id: Int?
    var title: String?
    var originalTitle: String?
    var originalName: String?
    var overview: String?
    var posterPath: String?
    var backdropPath: String?
    var voteAvg: Double?
    var voteCount: Double?
    var releaseDate: String?
    
    var posterData: Data?
    
    init() {}

    required init?(map: Map) {}

    func mapping(map: Map) {
        id <- map["activeAt"]
        overview <- map["overview"]
        posterPath <- map["poster_path"]
        backdropPath <- map["backdrop_path"]
        voteCount <- map["vote_count"]
        voteAvg <- map["vote_average"]
        title <- map["title"]
        originalTitle <- map["original_title"]
        originalName <- map["original_name"]
        releaseDate <- map["release_date"]
    }
    
    static func == (lhs: MovieModel, rhs: MovieModel) -> Bool {
        return lhs.title == rhs.title
    }
    
    func getReleaseDate() -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter.date(from: self.releaseDate ?? "")
    }
    
    func getDisplayableTitle() -> String {
        if title?.isEmpty == false {
            return title!
        } else if originalTitle?.isEmpty == false {
            return originalTitle!
        } else if originalName?.isEmpty == false {
            return originalName!
        } else {
            return ""
        }
    }
}



