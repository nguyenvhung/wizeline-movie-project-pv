//
//  MovieModel+Realm.swift
//  WizelineMovieApp
//
//  Created by hungnguy on 4/29/22.
//

import Foundation
import Realm
import RealmSwift

class MovieRealmModel: Object {
    @objc dynamic var title: String = ""
    @objc dynamic var overview: String = ""
    @objc dynamic var photoData: Data? = nil
    
    func toMovieModel() -> MovieModel {
        let movie = MovieModel()
        movie.overview = overview
        movie.title = title
        movie.posterData = photoData
        
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "yyyy-MM-dd"
        movie.releaseDate = dateFormatterPrint.string(from: Date())
        
        return movie
    }
    
    func setPosterDataFromUIImage(image: UIImage) {
        let data = image.jpegData(compressionQuality: 1.0)
        self.photoData = data
        debugPrint("data = \(self.photoData)")
    }
}
