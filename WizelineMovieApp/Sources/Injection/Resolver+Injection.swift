//
//  Resolver+Injection.swift
//  survey_hung
//
//  Created by Hung Nguyen on 14/09/2021.
//

import Foundation
import Resolver

extension Resolver: ResolverRegistering {

    public static func registerAllServices() {
        defaultScope = .graph

        register(KeychainProtocol.self) { Keychain.default }.scope(.application)
        register(NetworkAPIProtocol.self) { NetworkAPI() }.scope(.application)
        register(AuthenticatedNetworkAPIProtocol.self) {
            AuthenticatedNetworkAPI(keychain: resolve())
        }.scope(.application)

        registerRepositories()
        registerUseCases()
        registerViewModels()
    }

    private static func registerRepositories() {
        register(MovieRepositoryProtocol.self) {
            MovieRepository(networkAPI: resolve(), authenticatedNetworkAPI: resolve())
        }
         
       
        
        register(ImageConfigRepositoryProtocol.self) {
            ImageConfigRepository(networkAPI: resolve(), authenticatedNetworkAPI: resolve())
        }
        
        register(LocalMoviesRepositoryProtocol.self) {
            LocalMoviesRepository()
        }
    }
    
    private static func registerUseCases() {
        register(ImageConfigUseCaseProtocol.self) {
            ImageConfigUseCase(repository: resolve())
        }
         
        register(MoviesUseCasesProtocol.self) {
            MoviesUseCases(
                remoteRepository: resolve(),
                localDatabaseRepository: resolve()
            )
        }
         
    }

    private static func registerViewModels() {
        register(TrendingListViewModelProtocol.self) { TrendingListViewModel() }.scope(.shared)
    }
}
